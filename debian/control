Source: golang-gopkg-eapache-queue.v1
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hpe.com>
Build-Depends: debhelper (>= 9),
               dh-golang,
               golang-go
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-gopkg-eapache-queue.v1.git
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-gopkg-eapache-queue.v1
Homepage: https://github.com/eapache/queue
XS-Go-Import-Path: gopkg.in/eapache/queue.v1

Package: golang-gopkg-eapache-queue.v1-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: fast golang queue using ring-buffer
 A fast Golang queue using a ring-buffer, based on the version suggested
 by Dariusz Górecki. Using this instead of other, simpler, queue
 implementations (slice+append or linked list) provides substantial memory
 and time benefits, and fewer GC pauses. The queue implemented here is as
 fast as it is in part because it is not thread-safe.
